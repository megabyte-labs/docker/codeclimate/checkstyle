# codeclimate-checkstyle
A code climate engine for running checkstyle on your java projects.

### Sample .codeclimate.yml configuration
```yaml
engines:
  checkstyle:
    enabled: true
    config: 'config/checkstyle/checkstyle.xml'
```

By default the engine runs the [code climate checker](https://github.com/sivakumar-kailasam/codeclimate-checkstyle/blob/master/config/codeclimate_checkstyle.xml) against your code if the config property is not defined

## ➤ Requirements

- **[Docker](https://gitlab.com/megabyte-labs/ansible-roles/docker)**
- [CodeClimate CLI](https://github.com/codeclimate/codeclimate)

### Optional Requirements

- [DockerSlim](https://gitlab.com/megabyte-labs/ansible-roles/dockerslim) - Used for generating compact, secure images
- [Google's Container structure test](https://github.com/GoogleContainerTools/container-structure-test) - For testing the Docker images


### Building the Docker Container

Run the below make command from the root of this repository to create a local fat docker image
```shell
make image
```

### Building a Slim Container

Run the below make command from the root of this repository to create a local slim docker image
```shell
make slim
```


### Test

Run the below command from the root of this repository to test the images created by this repository.
```shell
make test
```
