.PHONY: image test

IMAGE_NAME ?= codeclimate/codeclimate-checkstyle

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-checkstyle:slim

DOCKER_RUN_MOUNTED = docker run --rm --user=root -w /usr/src/app -v $(PWD):/usr/src/app

image:
	docker build --rm -t $(IMAGE_NAME) .

Gemfile.lock: image
	$(DOCKER_RUN_MOUNTED) --user root $(IMAGE_NAME) bundle install

#test: image
#	$(DOCKER_RUN_MOUNTED) $(IMAGE_NAME) rspec


upgrade:
	$(DOCKER_RUN_MOUNTED) $(IMAGE_NAME) ./bin/upgrade.sh
	$(DOCKER_RUN_MOUNTED) $(IMAGE_NAME) ./bin/scrape-docs

slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec '/usr/src/app/bin/codeclimate-checkstyle || continue' --mount "$$PWD:/code" --workdir '/code' --preserve-path-file 'paths.txt' $(IMAGE_NAME) && prettier --write slim.report.json 

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml
